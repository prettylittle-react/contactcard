import React from 'react';
import PropTypes from 'prop-types';

//import Text from 'text';
//import Heading from 'heading';
import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import Link from 'link';

import {encode} from 'libs/string';

import './ContactCard.scss';

/**
 * ContactCard
 * @description [Description]
 * @example
  <div id="ContactCard"></div>
  <script>
    ReactDOM.render(React.createElement(Components.ContactCard, {
    	title : 'Example ContactCard'
    }), document.getElementById("ContactCard"));
  </script>
 */
const ContactCard = props => {
	const baseClass = 'contact-card';
	const {title, address, tel, email} = props;

	return (
		<div className={`${baseClass}`}>
			<Heading content={title} h={4} />
			<div>
				<Text content={address} />
				<Text content={tel} />
				<Link label={encode(email)} href={`mailto:${email}`} />
			</div>
		</div>
	);
};

ContactCard.defaultProps = {
	title: '',
	address: '',
	tel: '',
	email: ''
};

ContactCard.propTypes = {
	title: PropTypes.string,
	address: PropTypes.string,
	tel: PropTypes.string,
	email: PropTypes.string
};

export default ContactCard;
